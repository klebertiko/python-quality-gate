import subprocess
from sys import exit

output = subprocess.run(['pytest',
                         '--cov=.',
                         '--cov-config=gitlab-ci-scripts/pytest/.pytest-cov.rc',
                         '--cov-fail-under=70',
                         '--ignore=.packages/'])

if output.returncode != 0:
    exit(output.returncode)
