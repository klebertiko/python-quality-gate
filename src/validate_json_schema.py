from jsonschema import validate
import logging
import subprocess

logging.basicConfig(level=logging.INFO)


def validate_schema(json_dict, schema_dict):
    """
    Validate JSON file schema​
    :param json_dict​
    :param schema_dict​
    :return: True when validation success or False when validation failed
    """
    try:
        subprocess.run(['echo', '$PATH'])
        validate(instance=json_dict, schema=schema_dict)
        logging.info(f'Validation success.')
        return True
    except Exception as e:
        logging.exception(f'An unexpected exception was faced during validation. Error: {e}')
        return False