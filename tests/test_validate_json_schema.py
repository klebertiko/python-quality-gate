import src.validate_json_schema as validate_json_schema


def test_validate_valid_json_valid_schema():
    """
    Validate JSON schema
    Returns True when schema and instance are validated
    """
    schema = {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
        },
    }
    assert validate_json_schema.validate_schema({"name": "foo"}, schema)
